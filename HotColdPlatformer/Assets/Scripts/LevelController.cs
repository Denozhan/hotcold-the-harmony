﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public Canvas lefSlide;
    // Start is called before the first frame update
    void Start()
    {
        lefSlide.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            yield return StartCoroutine(slideAnimation());
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }

    private IEnumerator slideAnimation()
    {
        lefSlide.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.75f);
    }
}
