﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour
{
    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;
    public Text text5;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return StartCoroutine(EffectTypeWriter("Welcome to Hotcold: The Harmony", text1));
        yield return StartCoroutine(EffectTypeWriter("Use Right&Left keys to move.", text2));;
        yield return StartCoroutine(EffectTypeWriter("Use Space to change temperature.", text3));
        yield return StartCoroutine(EffectTypeWriter("Use Up key to jump.", text4));
        yield return StartCoroutine(EffectTypeWriter("You can use temperature  to pass blocks.", text5));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator EffectTypeWriter(string text, Text desiredText)
    {
        foreach (char character in text.ToCharArray())
        {
            desiredText.text = desiredText.text + character;
            yield return new WaitForSeconds(0.15f);
        }
    }
}
