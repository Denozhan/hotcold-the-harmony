﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotPlatformBehav : MonoBehaviour
{
    public static HotPlatformBehav hotBehavPlatform;

    private BoxCollider2D boxCollider;
    private GameObject[] allPlatforms;
    void Start()
    {
        hotBehavPlatform = GetComponent<HotPlatformBehav>();
        allPlatforms = GameObject.FindGameObjectsWithTag("HotPlatform");
    }

    public void controlCollider()
    {
        foreach (GameObject gobject in allPlatforms)
        {
            gobject.GetComponent<BoxCollider2D>().enabled = !gobject.GetComponent<BoxCollider2D>().enabled;
        }
    }
}
