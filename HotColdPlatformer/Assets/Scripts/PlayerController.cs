﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;
    public GameObject sceneLight;
    public GameObject hotLight;
    public GameObject coldLight;
    public AudioSource coldSFX;
    public AudioSource hotSFX;
    public GameObject playerStart;
    public static PlayerController playerController;
    

    private Rigidbody2D _playerRigid;
    private bool _isGrounded;
    private Animator _animator;
    private float _velocX;
    private Transform _startPosition;

    // Start is called before the first frame update
    void Start()
    {
        _startPosition = playerStart.GetComponent<Transform>();
        playerController = GetComponent<PlayerController>();
        MusicController.musicController.PlayMusic();
        _animator = transform.Find("AnimatorChild").GetComponent<Animator>();
        _isGrounded = false;
        _playerRigid = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        _velocX = Input.GetAxisRaw("Horizontal");

        if((_velocX > 0 || _velocX < 0) && _isGrounded)
        {
            _animator.SetBool("isRunning", true);
        }
        else
        {
            _animator.SetBool("isRunning", false);
        }

        if (_velocX < 0)
        {
            _playerRigid.velocity = new Vector2(-moveSpeed, _playerRigid.velocity.y);
        }
        if (_velocX > 0)
        {
            _playerRigid.velocity = new Vector2(moveSpeed, _playerRigid.velocity.y);
        }

        if(Input.GetKeyDown("space") && sceneLight.gameObject.activeSelf)
        {
            sceneLight.gameObject.SetActive(false);
            coldLight.gameObject.SetActive(true);
            coldSFX.Play();
            EffectPlayer.effectControl.PlayEffectChange();
            if (ColdPlatformBehav.coldBehavPlatform != null) { ColdPlatformBehav.coldBehavPlatform.controlCollider(); }
            if(WaterBlock.waterBlock != null)
            {
                WaterBlock.waterBlock.FreezeWater();
            }
        }
        else if (Input.GetKeyDown("space") && coldLight.gameObject.activeSelf)
        {
            coldLight.gameObject.SetActive(false);
            hotLight.gameObject.SetActive(true);
            hotSFX.Play();
            if (ColdPlatformBehav.coldBehavPlatform != null) { ColdPlatformBehav.coldBehavPlatform.controlCollider(); }
            if (HotPlatformBehav.hotBehavPlatform != null) { HotPlatformBehav.hotBehavPlatform.controlCollider(); }

            EffectPlayer.effectControl.PlayEffectChange();
            if (IceBlock.iceBlock != null)
            {
                IceBlock.iceBlock.IceMelt();
            }
            if (WaterBlock.waterBlock != null)
            {
                WaterBlock.waterBlock.FreezeWater();
            }
        }
        else if (Input.GetKeyDown("space") && hotLight.gameObject.activeSelf)
        {
            hotLight.gameObject.SetActive(false);
            sceneLight.gameObject.SetActive(true);
            if (HotPlatformBehav.hotBehavPlatform != null) { HotPlatformBehav.hotBehavPlatform.controlCollider(); }

        }

        if (Input.GetKey("up") && _isGrounded)
        {
            _playerRigid.velocity = new Vector2(_playerRigid.velocity.x, 0f);
            _playerRigid.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            _isGrounded = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Platform" || collision.gameObject.tag == "HotPlatform" || collision.gameObject.tag == "ColdPlatform" || collision.gameObject.tag == "Wall")
        {
            _isGrounded = true;
            _playerRigid.velocity = Vector2.zero;
            _playerRigid.angularVelocity = 0f;
        }
    }

    public void GoToStartPosition()
    {
        GetComponent<Transform>().position = _startPosition.position;
    }
}
