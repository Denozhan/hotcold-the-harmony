﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject imageToDelete;
    void Start()
    {
        //Screen.fullScreen = !Screen.fullScreen;
        //Screen.SetResolution(1024, 768, false); 
        StartCoroutine("deleteWithTime");
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    private IEnumerator deleteWithTime()
    {
        yield return StartCoroutine("deleteNow");
        Destroy(imageToDelete.gameObject);
    }

    public void StartButtonClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    IEnumerator deleteNow()
    {
        yield return new WaitForSeconds(0.25f);
    }

}
