﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColdPlatformBehav : MonoBehaviour
{
    public static ColdPlatformBehav coldBehavPlatform;

    private BoxCollider2D boxCollider;
    private GameObject[] allPlatforms;
    void Start()
    {
        coldBehavPlatform = GetComponent<ColdPlatformBehav>();
        allPlatforms = GameObject.FindGameObjectsWithTag("ColdPlatform");
    }

    public void controlCollider()
    {
        foreach (GameObject gobject in allPlatforms)
        {
            gobject.GetComponent<BoxCollider2D>().enabled = !gobject.GetComponent<BoxCollider2D>().enabled;
        }
    }
}
