﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceBlock : MonoBehaviour
{
    public GameObject iceBlockSFX;
    public static IceBlock iceBlock;

    private GameObject[] _iceBlocks;
    // Start is called before the first frame update
    void Start()
    {
        _iceBlocks = GameObject.FindGameObjectsWithTag("IceBlock");
        iceBlock = GetComponent<IceBlock>();
    }

    public void IceMelt()
    {
        foreach(GameObject gobject in _iceBlocks)
        {
            GameObject go = Instantiate(iceBlockSFX, transform.position, Quaternion.identity);
            go.GetComponent<ParticleSystem>().Play();
            Destroy(go, 1.0f);
            Destroy(gobject);
        }
    }
}
