﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPlayer : MonoBehaviour
{
    public ParticleSystem effectChange;
    public GameObject playPosition;
    public static EffectPlayer effectControl;

    private Vector2 _playVector;
    // Start is called before the first frame update
    void Start()
    {
        effectControl = GetComponent<EffectPlayer>();
        _playVector = playPosition.GetComponent<Transform>().position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayEffectChange()
    {
        effectChange.Play();
    }
}
