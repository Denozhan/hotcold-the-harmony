﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBlock : MonoBehaviour
{
    public static WaterBlock waterBlock;

    private BoxCollider2D _boxCollider2D;
    void Start()
    {
        waterBlock = GetComponent<WaterBlock>();
        _boxCollider2D = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FreezeWater()
    {
        _boxCollider2D.enabled = !_boxCollider2D.enabled;
    } 
      
}
