# HOT COLD: THE HARMONY
#### Temperature is the key..

[![N|Solid](https://img.itch.zone/aW1hZ2UvMTAzODEyMC81OTI3OTU3LnBuZw==/347x500/7mGwkE.png)](https://denzhn.itch.io/hotcold-the-harmony)

Welcome to Hotcold: The Harmony. This game was developed for Level Up Circle: Beginner Jam #1.
Your purpose is reach the harmony to complete the game. There are a lot of diffuculties along the way, just change the temperature and overcome them. By passing levels one by one, you will get close the harmony. Just remember that difference is good don't blame it, use it for your purpose wisely.

## Features

- Changeable temperature, makes releated blocks transparent
- Wall Jump mechanic (still needs to be improved)
- Post processing effects
- 20 levels with puzzles

## Ranked 82nd with 6 ratings (Score: 3.139)

Game jam has 136 entries totally. With 6 ratings i got the 82nd place.

| Criteria | Rank | Score* | Raw Score |
| ------ | ------ | ------ | ------ |
| Originality | #67 | 3.608 | 4.167 |
| Use Of Theme | #73 | 3.320 | 3.833 |
| Fun | #75 | 2.887 | 3.333 |
| Overall| 	#82 | 3.139 | 3.625 |
| Community Spirit | #97| 2.742 | 3.167 |


## License

Project is open-source. You can use it like you want. **But don't forget to mention me at credits!**

